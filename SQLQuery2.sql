/*2.	Teniendo en cuenta el modelo de base de datos, realice una consulta SQL que presente las siguientes columnas: 
a.	Placa del veh�culo
b.	Tipo de identificaci�n de la empresa
c.	N�mero de identificaci�n de la empresa
d.	Nombre de la empresa
e.	Cantidad de conductores vinculados al veh�culo
La consulta debe estar ordenada por el campo �Placa del veh�culo� y 
solo debe mostrar los veh�culos que tengan m�s de dos conductores vinculados.
*/
SELECT  veh.placa,emp.tipoIdentificacion,emp.numeroIdentificacion,
        emp.nombreCompleto ,count(cond.id_vehiculo) as  CantidadConductoresVinculadosVehiculo
FROM  Empresa emp 
JOIN  Vehiculo veh ON   emp.id_empresa=veh.id_empresa
JOIN Conductor cond ON   veh.id_vehiculo=cond.id_vehiculo
group by veh.placa,emp.tipoIdentificacion,emp.numeroIdentificacion,
        emp.nombreCompleto
HAVING COUNT(cond.id_vehiculo)>=2
ORDER BY veh.placa 