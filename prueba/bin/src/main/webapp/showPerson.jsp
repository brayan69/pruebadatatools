<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="ISO-8859-1">
            <title>Prueba</title>
        </head>

        <body>
            <h2>Formulario de vista/consulta de registros</h2>
            <form action="updatePerson" align="center">
                <table border="1" align="center">
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Procesado</th>
                        <th>Seleccionar</th>
                    </tr>
                    <c:forEach var="listValue" items="${personList}">
                        <tr>
                            <td name="name">${listValue.name}</td>
                            <td  name="apellido">${listValue.apellido}</td>
                            <td  name="procesado">${listValue.procesado}</td>
                                  <td><input type="checkbox" name="id" value="${listValue.id}"></td>
                        </tr>
                    </c:forEach>
                </table>
                <br>
                <input type="submit" value="Procesar">
            </form>

        </body>

        </html>