package com.prueba.demo.controller;

import org.springframework.stereotype.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.prueba.demo.model.RepresentanteLegal;
import com.prueba.demo.dao.RepresentanteLegalRepo;
import com.prueba.demo.model.Empresa;
import com.prueba.demo.dao.EmpresaRepo;

@Controller
public class TransporteController {

    @Autowired
    RepresentanteLegalRepo repo;

    @Autowired
    EmpresaRepo repo1;

    @RequestMapping("/")
    public String home() {
        return "home.jsp";
    }

    @RequestMapping("/addRepresentanteLegal")
    public String addRepresentanteLegal(RepresentanteLegal representanteLegal) {
        repo.save(representanteLegal);

        return "adminRepresentantelegal.jsp";
    }

    @RequestMapping("/addEmpresa")
    public ModelAndView addEmpresa(Empresa empresa) {
        repo1.save(empresa);

        ModelAndView mv = new ModelAndView("adminEmpresa.jsp");
        List<RepresentanteLegal> representanteLegal = (List<RepresentanteLegal>) repo.findAll();
        mv.addObject(representanteLegal);
        System.out.println("hpla" + mv);
        return mv;
    }

    /* @RequestMapping("/updatePerson")
    public ModelAndView updatePerson(String id) {
        if (id != null) {
            String[] partsId = id.split(",");
            for (int i = 0; i < partsId.length; i++) {

                RepresentanteLegal personUpdate = repo.findById(Integer.parseInt(partsId[i])).get();
                personUpdate.setProcesado("true");
                repo.save(personUpdate);

            }
        }

        ModelAndView mv = new ModelAndView("showPerson.jsp");
        List<RepresentanteLegal> person = (List<RepresentanteLegal>) repo.findAll();
        mv.addObject(person);
        return mv;
    }*/
    @RequestMapping("/getRepresentanteLegal")
    public ModelAndView getRepresentanteLegal() {
        ModelAndView mv = new ModelAndView("showRepresentanteLegal.jsp");
        List<RepresentanteLegal> representanteLegal = (List<RepresentanteLegal>) repo.findAll();
        mv.addObject(representanteLegal);
        System.out.print("representanteLegal" + mv);
        return mv;
    }

    @RequestMapping("/getEmpresa")
    public ModelAndView getEmpresa() {
        ModelAndView mv = new ModelAndView("showEmpresa.jsp");
        List<Empresa> representanteLegal = (List<Empresa>) repo1.findAll();
        mv.addObject(representanteLegal);
        System.out.println("hpla" + mv);
        return mv;
    }

    @RequestMapping("/getRepresentanteLegalEmpresa")
    public ModelAndView getRepresentanteLegalEmpresa() {
        ModelAndView mv = new ModelAndView("adminEmpresa.jsp");
        List<RepresentanteLegal> representanteLegal = (List<RepresentanteLegal>) repo.findAll();
        mv.addObject(representanteLegal);
        System.out.println("hpla" + mv);
        return mv;
    }
}
