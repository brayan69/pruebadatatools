package com.prueba.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.prueba.demo.model.Empresa;

public interface EmpresaRepo extends CrudRepository<Empresa,Integer>
{

}
