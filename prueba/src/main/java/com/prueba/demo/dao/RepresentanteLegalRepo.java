package com.prueba.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.prueba.demo.model.RepresentanteLegal;

public interface RepresentanteLegalRepo extends CrudRepository<RepresentanteLegal,Integer>
{

}
