package com.prueba.demo.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Empresa implements Serializable {
@Id	
@GeneratedValue(strategy=GenerationType.IDENTITY)int id_Empresa;
        private String tipoIdentificacion;
	private int numeroIdentificacion;
	private String direccion;
	private String nombrecompleto;
        private String ciudad;
        private String departamento;
	private String pais;
        private String telefono;
        private int id_representante;

    public int getId_representante() {
        return id_representante;
    }

    public void setId_representante(int id_representante) {
        this.id_representante = id_representante;
    }

    public String getNombrecompleto() {
        return nombrecompleto;
    }

    public void setNombrecompleto(String nombrecompleto) {
        this.nombrecompleto = nombrecompleto;
    }
    

    public int getId_Empresa() {
        return id_Empresa;
    }

    public void setId_Empresa(int id_Empresa) {
        this.id_Empresa = id_Empresa;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public int getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(int numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
	
	
	
	@Override
	public String toString() {
		return "tipoIdentificacion=" + tipoIdentificacion +
                        ", numeroIdentificacion=" + numeroIdentificacion+ ", "
                        + "nombrecompleto=" + nombrecompleto +","
                        + "direccion=" + direccion +","
                        + "ciudad=" + ciudad +","
                        + "departamento=" + departamento+","
                        + "pais=" + pais +","
                        + "telefono=" + telefono+","
                        + "id_representante=" + id_representante+","
                        +id_Empresa+ "";
	}



}
