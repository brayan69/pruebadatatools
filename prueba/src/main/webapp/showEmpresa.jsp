<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="ISO-8859-1">
        <title>Prueba</title>
        <link rel="stylesheet" href="assets/styles/home.css">
    </head>

    <body>
        <form action="/getEmpresa" id="opcionesAdministrador">
            <h2>Formulario de vista/consulta de registros</h2>
            <input type="button" id="submit" onclick="location.href = 'home.jsp';" value="Home" />
        </form>

        <form align="center" class="form">
            <table border="1" align="center">
                <tr>
                    <th>Tipo documento</th>
                    <th>N�mero de documento de identificaci�n</th>
                    <th>Nombre completo</th>
                    <th>Direcci�n</th>
                    <th>Ciudad</th>
                    <th>Departamento</th>
                    <th>Pa�s</th>
                    <th>Tel�fono</th>
                </tr>
                <c:forEach var="listValue" items="${empresaList}">
                    <tr>
                        <td name="tipoIdentificacion">${listValue.tipoIdentificacion}</td>
                        <td  name="numeroIdentificacion">${listValue.numeroIdentificacion}</td>
                        <td  name="nombrecompleto">${listValue.nombrecompleto}</td>
                        <td  name="direccion">${listValue.direccion}</td>
                        <td  name="ciudad">${listValue.ciudad}</td>
                        <td  name="departamento">${listValue.departamento}</td>
                        <td  name="pais">${listValue.pais}</td>
                        <td  name="telefono">${listValue.telefono}</td>
                    </tr>
                </c:forEach>
            </table>
            <br>

        </form>

    </body>

</html>